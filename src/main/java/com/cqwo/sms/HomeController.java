package com.cqwo.sms;


import com.cqwo.sms.helper.HttpHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by cqnews on 2017/3/23.
 */
@RestController
public class HomeController {


    /**
     * 编码
     */
    private String encoding = "utf-8";

    private Lock lock = new ReentrantLock();

    Logger logger = LoggerFactory.getLogger(HomeController.class);

    @RequestMapping(value = "/")
    public String index() throws Exception {

        send("18983071151", "测试消息");

        return "测试知道";
    }


    public Boolean send(String to, String body) throws Exception {


        lock.lock();
        SMSConfigInfo configInfo = new SMSConfigInfo();

        body = MessageFormat.format("【{1}】{0}", body, configInfo.sign);
        try {
            String url = String.format("%s?action=send&userid=%d&account=%s&password=%s&mobile=%s&content=%s&sendTime=&extno=", configInfo.url, configInfo.userId, configInfo.account, configInfo.password, to, URLEncoder.encode(body, encoding));

            logger.info("发送短信:" + url);
            HttpHelper.doGet(url);

        } catch (Exception e) {

            lock.unlock();
            return false;
        }
        lock.unlock();
        return true;

    }


    class SMSConfigInfo {

        /**
         * 短信服务器地址
         */
        String url = "http://xxxx.com/sms.aspx";//短信服务器地址
        /**
         * 企业id
         */
        int userId = 304;//企业id
        /**
         * 短信账号
         */
        String account = "cnyhn";//短信账号
        /**
         * 短信密码
         */
        String password = "123333";//短信密码
        /**
         * 短信签名
         */
        String sign = "杨红娘婚介";//短信签名


    }
}

